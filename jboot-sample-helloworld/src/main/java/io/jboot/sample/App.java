package io.jboot.sample;

import io.jboot.Jboot;

/**
 *
 * 默认信息
 * JbootConfig {version='1.7.0', mode='dev', bannerEnable=true, bannerFile='banner.txt', jfinalConfig='io.jboot.web.JbootAppConfig'}
 * JbootServerConfig {type='undertow', host='0.0.0.0', port=8080, contextPath='/'}
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        Jboot.run(args);
    }
}
