package io.jboot.sample;

import io.jboot.Jboot;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        //Jboot.setBootArg("jboot.server.type", "jetty");
        Jboot.setBootArg("jboot.metric.url", "/metrics_admin");
        Jboot.setBootArg("jboot.metric.reporter", "slf4j");
        Jboot.run(args);
    }
}
