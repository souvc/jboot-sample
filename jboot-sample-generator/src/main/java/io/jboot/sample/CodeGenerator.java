package io.jboot.sample;

import io.jboot.codegen.model.JbootModelGenerator;
import io.jboot.codegen.service.JbootServiceGenerator;

/**
 * 使用代码生成器
 * Jboot 内置了一个简易的代码生成器，通过代码生成器运行，
 * Jboot帮开发者生成每个数据库表对应 java 的 model 实体类，同时可以生成带有增、删、改、查基本数据库操作能力的 service 层代码。
 */
public class CodeGenerator {

    public static void main(String args[]) {
        //依赖model的包名
        String modelPackage = "io.jboot.user.model";
        //生成service 的包名
        String basePackage = "io.jboot.user.service";

        JbootModelGenerator.run(modelPackage);
        JbootServiceGenerator.run(basePackage, modelPackage);

    }
}