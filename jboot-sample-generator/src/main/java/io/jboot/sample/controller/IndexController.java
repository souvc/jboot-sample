/**
 * Copyright (c) 2015-2018, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jboot.sample.controller;

import com.google.inject.Inject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import io.jboot.user.model.User;
import io.jboot.user.service.UserService;
import io.jboot.web.controller.JbootController;
import io.jboot.web.controller.annotation.RequestMapping;

import java.util.Arrays;
import java.util.List;

/**
 * 测试直接输出字符串hello world
 */
@RequestMapping("/")
public class IndexController extends JbootController {

    @Inject
    private UserService userService;

    public void index() {
        renderJson("hello world");
    }

    public void dbtest(){
        List<Record> records = Db.find("select * from user");
        renderJson(records);
    }

    public void users() {
        // 这里用到了 userService 的查询方法
        List<User> users = userService.findAll();
        renderJson(users);
    }

}



