/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : localhost:3306
 Source Schema         : jbootdemo

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 27/10/2018 11:11:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `login_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登陆名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'name', 'password');
INSERT INTO `user` VALUES (2, 'name2', 'password2');
INSERT INTO `user` VALUES (3, 'name3', 'pass');

SET FOREIGN_KEY_CHECKS = 1;


-- 生成代码之后重新添加一个字段测试

-- 需要注意的是： 再次运行该代码生成器的时候，BaseUser会被重新覆盖，其他代码不会被覆盖。
-- 若需要重新生成 service 层 和 User 等代码，需要手动删除后，再次运行代码生成器 CodeGenerator

ALTER TABLE `user`
ADD COLUMN `create_time` datetime(0) NULL COMMENT '创建时间';







