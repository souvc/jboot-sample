package io.jboot.sample;

import io.jboot.Jboot;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        Jboot.run(args);
    }
}
