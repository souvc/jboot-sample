/**
 * Copyright (c) 2015-2018, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jboot.sample.controller;

import com.google.inject.Inject;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.user.model.User;
import io.jboot.user.service.UserService;
import io.jboot.web.controller.JbootController;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 测试直接输出字符串hello world
 */
@RequestMapping("/user")
public class IndexController extends JbootController {

    @Inject
    private UserService userService;

    /**
     * getParaToInt() 可以获得request提交过来的page数据，例如：http://127.0.0.1:8080/user?page=100 ，此时，代码里 page 的值是 100，当不传值得时候，默认值是 1 。
     * 通过 userService.paginate 查询数据库，返回一个 Page 对象，里面包含了 当前页码、总页码 和 数据列表等信息。
     * 通过 setAttr() 把数据结果传递到页面
     */
    public void index() {
        int page = getParaToInt("page", 1);
        Page<User> userPage = (Page<User>) userService.paginate(page, 10);
        setAttr("pageData", userPage);
        render("/user.html");
    }

    public void add() {
        int id = getParaToInt("id", 0);
        if (id > 0) { //有id ，说明有数据提交过来，用来做修改的标识。
            setAttr("id", id);
        }
        render("/add.html");
    }

    /**
     * doSave() 方法的主要作用是接收数据、把数据保存到数据库、然后跳转到 /user 这个页面去。
     */
    public void doSave() {
        String loginName = getPara("login_name");
        String password = getPara("password");

        User user = new User();
        user.setLoginName(loginName);
        user.setPassword(password);

        userService.save(user);

        redirect("/user");
    }

    /**
     * 删除
     */
    public void del() {
        long id = getParaToLong("id", 0l);
        userService.deleteById(id);
        redirect("/user");
    }

}



