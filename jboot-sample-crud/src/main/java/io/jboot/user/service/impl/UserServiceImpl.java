package io.jboot.user.service.impl;

import io.jboot.aop.annotation.Bean;
import io.jboot.user.service.UserService;
import io.jboot.user.model.User;
import io.jboot.service.JbootServiceBase;

import javax.inject.Singleton;

/**
 * 自动注入
 * Jboot 通过 Google Guice 提供了强健稳定的代码注入功能，使用注入功能只需要了解一下三个注解：
 *
 * @Bean : 声明此类可以被自动注入
 * @Inject : 对属性进行赋值注入
 * @Singleton : 申明此类为单例模式，通常和@Bean 一起使用。
 */
@Bean
@Singleton
public class UserServiceImpl extends JbootServiceBase<User> implements UserService {

}